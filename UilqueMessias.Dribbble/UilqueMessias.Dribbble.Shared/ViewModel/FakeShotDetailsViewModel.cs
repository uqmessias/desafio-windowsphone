﻿using System;
using System.Collections.Generic;
using System.Text;
using UilqueMessias.Dribbble.Model;

namespace UilqueMessias.Dribbble.ViewModel
{
    public class FakeShotDetailsViewModel : ShotDetailsViewModel
    {
        public FakeShotDetailsViewModel()
        {
#if DEBUG
            this.FillItem();
#endif
        }

        private void FillItem()
        {
            string[] nomes = new string[] { "José", "Antônio", "Pedro", "Uilque", "Joaquim", "Manoel", "Marcos", "Pedro" },
                     sobreNomes = new string[] { "Pereira", "Ferreira", "Oliveira", "Farias", "Caxias", "Malias", "Cruz", "Vieira" };
            int min = 2, max = 9999;
            string format = "{0} {1}";
            Random rnd = new Random();

            this.Shot = new Shot()
            {
                Height = 600,
                Width = 800,
                SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                MediumImageUrl = "ms-appx:///Assets/Images/img-teste-medium.jpg",
                LargeImageUrl = "ms-appx:///Assets/Images/img-teste-large.jpg",
                ViewsCount = rnd.Next(min, max),
                CommentsCount = rnd.Next(min, max),
                LikesCount = rnd.Next(min, max),
                Title = "BFF's TEST",
                Description = "<p>This is an description just for testing the UI</p><p>And this text is in <b>bold</b>, but I can also put it in <i>italic</i></p>",
                Url = "http://dribbble.com/shots/2157791-Man-Overboard",
                Player = new Model.Player
                {
                    Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                    AvatarUrl = "ms-appx:///Assets/Images/avatar-uilque.jpg",
                    Location = "São Paulo, Brazil",
                    TwitterNickName = nomes[rnd.Next(0, nomes.Length - 1)],
                    Url = "http://dribbble.com/bjohnson"
                    
                }
            };
        }
    }
}
