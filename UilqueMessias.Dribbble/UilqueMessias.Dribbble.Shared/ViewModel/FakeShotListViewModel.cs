﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace UilqueMessias.Dribbble.ViewModel
{
    public class FakeShotListViewModel : ShotListViewModel
    {
        public FakeShotListViewModel()
        {
#if DEBUG
            this.FillItems();

#endif
        }
        private async void FillItems()
        {
            this.Items = new Model.InfiniteNavigationCollection<Model.Shot>(async (count, pageNumber) =>
            {
                return this.GetItems();
            });
        }
        private List<Model.Shot> GetItems()
        {
            string[] nomes = new string[] { "José", "Antônio", "Pedro", "Uilque", "Joaquim", "Manoel", "Marcos", "Pedro" },
                     sobreNomes = new string[] { "Pereira", "Ferreira", "Oliveira", "Farias", "Caxias", "Malias", "Cruz", "Vieira" };
            int min = 2, max = 9999;
            string format = "{0} {1}";
            Random rnd = new Random();

            var result = new List<Model.Shot>
                {
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-uilque.jpg"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-teste.png"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-uilque.jpg"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-teste.png"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-uilque.jpg"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-teste.png"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-uilque.jpg"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-teste.png"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-uilque.jpg"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-teste.png"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-uilque.jpg"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-teste.png"
                        }
                    },
                    new Model.Shot
                    {
                        SmallImageUrl = "ms-appx:///Assets/Images/img-teste.jpg",
                        ViewsCount = rnd.Next(min, max),
                        CommentsCount = rnd.Next(min, max),
                        LikesCount = rnd.Next(min, max),
                        Player = new Model.Player
                        {
                            Name = String.Format(format, nomes[rnd.Next(0, nomes.Length - 1)], sobreNomes[rnd.Next(0, sobreNomes.Length - 1)]),
                            AvatarUrl = "ms-appx:///Assets/Images/avatar-uilque.jpg"
                        }
                    }
                };
            return result;
        }
    }
}
