﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using UilqueMessias.Dribbble.Model;
using UilqueMessias.Dribbble.Services;
using UilqueMessias.Dribbble.ViewModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace UilqueMessias.Dribbble
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DribbbleListPage : Page
    {
        private string _loadingState = "LoadingState",
                       _loadedState = "LoadedState",
                       _notConnected = "NotConnectedState";
        public DribbbleListPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Enabled;
            this.Loaded += PageLoaded;
            this.GoToVisualState(this._loadingState, true);
        }


        private async void PageLoaded(object sender, RoutedEventArgs e)
        {
            await this.TryGetShots();
        }

        private async Task<bool> TryGetShots()
        {
            if (ConnectionService.Instance.ConnectionType == ConnectionService.InternetConnection.NotConnected)
            {
                this.GoToVisualState(this._notConnected, true);
            }
            else
            {
                ShotListViewModel viewModel = this.DataContext as ShotListViewModel;

                if (viewModel != null)
                    viewModel.FillItem();
                await Task.Delay(200);
                this.GoToVisualState(this._loadedState, true);
                return true;
            }

            return false;
        }


        private async void TryAgain(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            ((Button)sender).IsEnabled = false;
            this.pbTrying.Visibility = Windows.UI.Xaml.Visibility.Visible;
            await Task.Delay(200);
            await this.TryGetShots();
            ((Button)sender).IsEnabled = true;
            this.pbTrying.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

        }
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Window.Current.SizeChanged += OnWindowSizeChanged;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            Window.Current.SizeChanged += OnWindowSizeChanged;

        }
        void OnWindowSizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            var b = Window.Current.Bounds;
            var s = e.Size;
        }

        public void HideProgressRing(FrameworkElement obj, string parentGridName = "grRoot", string progressRingName = "prLoading")
        {
            int maxSteps = 100;
            ProgressRing prLoading = null;

            while (maxSteps > 0 && obj != null)
            {
                if (obj is Grid && obj.Name == parentGridName)
                {
                    prLoading = ((Grid)obj).FindName(progressRingName) as ProgressRing;
                    break;
                }

                obj = obj.Parent as FrameworkElement;

                maxSteps--;
            }

            if (prLoading != null)
                prLoading.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void OnShotImageFailed(object sender, Windows.UI.Xaml.ExceptionRoutedEventArgs e)
        {
            Image img = sender as Image;

            if (img != null)
            {
                img.Source = new BitmapImage(new Uri("ms-appx:///Assets/Image/not-loaded-image.png", UriKind.Relative));
                this.HideProgressRing(img);
            }
        }

        private void OnShotImageOpened(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.HideProgressRing(sender as FrameworkElement);
        }
        private void OnShotSelected(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e)
        {
            if (this.lvDribbble.SelectedItem != null)
            {
                Dribbble.Services.DribbbleService.Instance.CurrentShot = this.lvDribbble.SelectedItem as Shot;

                if (Dribbble.Services.DribbbleService.Instance.CurrentShot != null)
                    this.Frame.Navigate(typeof(ShotDetailsPage));
            }
        }
        private void GoToVisualState(string visualStateName, bool useTransitions)
        {
            VisualStateManager.GoToState(this, visualStateName, useTransitions);
        }
    }
}
