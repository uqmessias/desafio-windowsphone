﻿using System;
using System.Collections.Generic;
using System.Text;
using UilqueMessias.Dribbble.Model;

namespace UilqueMessias.Dribbble.ViewModel
{
    public class ShotDetailsViewModel : ViewModelBase
    {
        private Shot _shot;
        public ShotDetailsViewModel()
        {
            this.FillItem();
        }

        public Shot Shot
        {
            get { return this._shot; }
            set
            {
                if (value != this._shot)
                {
                    this._shot = value;
                    this.OnPropertyChanged();
                }
            }
        }


        public void FillItem()
        {
            this.Shot = Dribbble.Services.DribbbleService.Instance.CurrentShot;
        }
    }
}
