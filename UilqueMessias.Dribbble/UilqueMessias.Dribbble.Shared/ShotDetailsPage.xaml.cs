﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UilqueMessias.Dribbble.ViewModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
#if WINDOWS_PHONE_APP
using Windows.Phone.UI.Input;
#endif
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace UilqueMessias.Dribbble
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ShotDetailsPage : Page
    {

        private string _loadingState = "LoadingState",
                       _loadedState = "LoadedState",
                       _loadedLandscapeState = "LoadedLandscapeState",
                       _loadedToUse = null;

        private double _x;
        private bool _goBack;
        private bool _loaded;

        public ShotDetailsPage()
        {
            this.InitializeComponent();
            this.Loaded += PageLoaded;
            this._loadedToUse = Window.Current.Bounds.Height <= Window.Current.Bounds.Width ? this._loadedLandscapeState : this._loadedState;
            VisualStateManager.GoToState(this, this._loadingState, true);
        }

        private async void PageLoaded(object sender, RoutedEventArgs e)
        {
            ShotDetailsViewModel viewModel = this.DataContext as ShotDetailsViewModel;

            if (viewModel != null)
                viewModel.FillItem();
            VisualStateManager.GoToState(this, this._loadedToUse, true);
            this._loaded = true;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
#if WINDOWS_PHONE_APP
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
#endif
            Window.Current.SizeChanged += OnWindowSizeChanged;
#if WINDOWS_APP
            this.appBarButton.Visibility = Visibility.Visible;
#endif
        }


        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
#if WINDOWS_PHONE_APP
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
#endif
            Window.Current.SizeChanged -= OnWindowSizeChanged;
        }
#if WINDOWS_PHONE_APP
        void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                e.Handled = true;
                this.Frame.GoBack();
            }
        }
#endif

        void OnWindowSizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            this.UpdateOrientation(e.Size.Width, e.Size.Height);
        }

        void UpdateOrientation(double width, double height)
        {
            if (height <= width)
            {
                this._loadedToUse = this._loadedLandscapeState;
            }
            else
                this._loadedToUse = this._loadedState;
            VisualStateManager.GoToState(this, this._loaded ? this._loadedToUse : this._loadingState, true);
        }

        private void OnBackButtonClick(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }
    }
}
