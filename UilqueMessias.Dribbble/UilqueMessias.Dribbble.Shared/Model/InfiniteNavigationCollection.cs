﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml.Data;

namespace UilqueMessias.Dribbble.Model
{
    public class InfiniteNavigationCollection<T> : ObservableCollection<T>, ISupportIncrementalLoading
    {
        private Func<uint, uint, Task<IList<T>>> _load;
        private uint _pageCounter;
        public bool HasMoreItems { get; protected set; }

        public InfiniteNavigationCollection(Func<uint, uint, Task<IList<T>>> load)
        {
            HasMoreItems = true;
            this._load = load;
            this._pageCounter = 1;
        }

        public IAsyncOperation<LoadMoreItemsResult> LoadMoreItemsAsync(uint count)
        {
            return AsyncInfo.Run(async c =>
            {
                var data = await this._load(count, this._pageCounter++);

                foreach (var item in data)
                {
                    Add(item);
                }

                HasMoreItems = data.Any();

                return new LoadMoreItemsResult()
                {
                    Count = data.Count > 0 ? (uint)data.Count : 0,
                };
            });
        }
    }
}
