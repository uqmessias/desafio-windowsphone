﻿using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UilqueMessias.Dribbble.Services;

namespace UilqueMessias.Dribbble.Model
{
    public class ShotCollection
    {
        [JsonProperty(PropertyName = "page")]
        public string PageNumber { get; set; }

        public uint CurrentPage
        {
            get
            {
                uint currentPage;
                uint.TryParse(this.PageNumber, out currentPage);
                return currentPage;
            }
        }

        [JsonProperty(PropertyName = "per_page")]
        public int ShotsPerPage { get; set; }

        [JsonProperty(PropertyName = "pages")]
        public int PageCount { get; set; }

        [JsonProperty(PropertyName = "total")]
        public int ShotCount { get; set; }

        [JsonProperty(PropertyName = "shots")]
        public ObservableCollection<Shot> Shots { get; set; }
    }

}
