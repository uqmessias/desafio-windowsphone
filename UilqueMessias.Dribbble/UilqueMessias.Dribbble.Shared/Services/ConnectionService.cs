﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Networking.Connectivity;

namespace UilqueMessias.Dribbble.Services
{
    class ConnectionService
    {

        private static ConnectionService _instance;

        public static ConnectionService Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ConnectionService();
                return _instance;
            }
        }

        public enum InternetConnection { Wireless, Wired, _3G, NotConnected };
        public InternetConnection ConnectionType
        {
            get
            {
                InternetConnection connectionType = InternetConnection.NotConnected;
                try
                {
                    ConnectionProfile prof = NetworkInformation.GetInternetConnectionProfile();

                    if (prof != null && prof.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess)
                    {
                        switch (prof.NetworkAdapter.IanaInterfaceType)
                        {
                            case 6:
                                connectionType = InternetConnection.Wired;
                                break;

                            case 71:
                                connectionType = InternetConnection.Wireless;
                                break;

                            default:
                                connectionType = InternetConnection._3G;
                                break;
                        }
                    }
                }
                catch { }

                return connectionType;
            }
        }
    }
}
