﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace UilqueMessias.Dribbble.Model
{
    public class Player
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "avatar_url")]
        public string AvatarUrl { get; set; }

        [JsonProperty(PropertyName = "twitter_screen_name")]
        public string TwitterNickName { get; set; }
    }
}
