﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using UilqueMessias.Dribbble.Model;

namespace UilqueMessias.Dribbble.ViewModel
{
    public class ShotListViewModel : ViewModelBase
    {
        private InfiniteNavigationCollection<Shot> _items;
        public ShotListViewModel()
        {
        }

        public InfiniteNavigationCollection<Shot> Items
        {
            get { return _items; }
            set
            {
                if (value != this._items)
                {
                    this._items = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public void FillItem()
        {
            this.Items = new Model.InfiniteNavigationCollection<Model.Shot>(async (count, pageNumber) =>
            {
                return (await Dribbble.Services.DribbbleService.Instance.GetShotsAsync(pageNumber)).Shots;
            });
        }
    }
}
