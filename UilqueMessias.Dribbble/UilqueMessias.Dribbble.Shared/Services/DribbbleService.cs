﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UilqueMessias.Dribbble.Model;
using Windows.Networking.BackgroundTransfer;
using Windows.Web.Http;

namespace UilqueMessias.Dribbble.Services
{
    public class DribbbleService
    {

        private static DribbbleService _instance;

        public static DribbbleService Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DribbbleService();
                return _instance;
            }
        }
        private enum ServiceType { }
        public const string BASE_DRIBBBLE_URI = "http://api.dribbble.com/shots/popular?page={0}";


        public Shot CurrentShot { get; set; }

        public async Task<ShotCollection> GetShotsAsync(uint pageNumber = 1)
        {
            using (HttpClient client = new HttpClient())
            {
                ShotCollection result = new ShotCollection();

                try
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new Windows.Web.Http.Headers.HttpMediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.GetAsync(new Uri(String.Format(BASE_DRIBBBLE_URI, pageNumber), UriKind.Absolute));

                    if (response.IsSuccessStatusCode)
                    {
                        var jsonResult = await response.Content.ReadAsStringAsync();

                        result = JsonConvert.DeserializeObject<ShotCollection>(jsonResult);
                    }
                }
                catch (Exception)
                {
                    result = new ShotCollection()
                    {
                        Shots = new System.Collections.ObjectModel.ObservableCollection<Shot>()
                    };
                }

                if (result == null || result.Shots == null)
                    result = new ShotCollection { Shots = new System.Collections.ObjectModel.ObservableCollection<Shot>() };

                return result;
            }
        }

    }
}
