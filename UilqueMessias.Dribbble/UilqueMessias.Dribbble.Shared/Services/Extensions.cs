﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;

namespace UilqueMessias.Dribbble.Services.Extensions
{
    public class HTMLToWebView
    {
        public static string GetHTML(DependencyObject obj)
        {
            return (string)obj.GetValue(HTMLProperty);
        }

        public static void SetHTML(DependencyObject obj, string value)
        {
            obj.SetValue(HTMLProperty, value);
        }

        // Using a DependencyProperty as the backing store for HTML.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HTMLProperty =
            DependencyProperty.RegisterAttached("HTML", typeof(string), typeof(HTMLToWebView),
            new PropertyMetadata(String.Empty, new PropertyChangedCallback((o,e) => HTMLToWebView.HTMLPropertyChanged(o, e.NewValue.ToString()))));

        private static void HTMLPropertyChanged(DependencyObject obj, string html)
        {
            WebView wv = obj as WebView;
            if(wv != null)
            {
                wv.NavigateToString(html);
            }
        }


    }

    public class StringToUpperCaseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (value ?? new object()).ToString().ToUpper();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

}
