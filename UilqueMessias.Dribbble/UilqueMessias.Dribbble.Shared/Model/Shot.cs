﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace UilqueMessias.Dribbble.Model
{
    public class Shot
    {
        //public int id { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "height")]
        public int Height { get; set; }

        [JsonProperty(PropertyName = "width")]
        public int Width { get; set; }

        [JsonProperty(PropertyName = "likes_count")]
        public int LikesCount { get; set; }

        [JsonProperty(PropertyName = "comments_count")]
        public int CommentsCount { get; set; }

        [JsonProperty(PropertyName = "views_count")]
        public int ViewsCount { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "short_url")]
        public string ShortUrl { get; set; }

        [JsonProperty(PropertyName = "image_teaser_url")]
        public string SmallImageUrl { get; set; }

        [JsonProperty(PropertyName = "image_400_url")]
        public string MediumImageUrl { get; set; }

        [JsonProperty(PropertyName = "image_url")]
        public string LargeImageUrl { get; set; }

        [JsonProperty(PropertyName = "player")]
        public Player Player { get; set; }
    }
}
